package com.site.blog.my.core.enums;

/**
 * @author xuwangcheng14@163.com
 * @version 1.0.0
 * @description
 * @date 2020/12/19  21:01
 */
public enum ThemeType {
    AMAZE("amaze"),
    DEFAULT_THEME("default"),
    JEKYLL("yummy-jekyll");

    private String themeName;

    ThemeType (String themeName) {
        this.themeName = themeName;
    }

    public String getThemeName() {
        return themeName;
    }
}
