package com.site.blog.my.core.enums;

/**
 * 文件存储类型
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/12/15 17:37
 */
public enum FileSaveType {
    LOCAL("1"),
    QI_NIU("2");

    private String type;

    FileSaveType (String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
