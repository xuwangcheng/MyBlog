package com.site.blog.my.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.site.blog.my.core.config.CommonLocalSetting;
import com.site.blog.my.core.enums.FileSaveType;
import com.site.blog.my.core.enums.SystemConfigName;
import lombok.Data;

import java.util.Date;

/**
 * 文件信息
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/25 14:23
 */
@Data
public class FileInfo {

    private Integer fileId;

    private String fileName;

    private String saveType;

    @JsonIgnore
    private String path;

    private String fileType;

    private Long size;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private String urlPath;

    @JsonIgnore
    private String diskPath;

    public void setPath (String path) {
        this.path = path;
    }

    public void setSaveType (String saveType) {
        this.saveType = saveType;
        this.setUrlPath();
    }

    public void setUrlPath () {
        if (this.urlPath == null) {
            if (this.path != null && this.saveType != null) {
                if (FileSaveType.LOCAL.getType().equals(this.saveType)) {
                    this.urlPath = CommonLocalSetting.getCustomSetting().getConfigValue(SystemConfigName.localFileDomain) + path.replace('\\', '/');
                    this.diskPath = CommonLocalSetting.getCustomSetting().getConfigValue(SystemConfigName.localFilePath) + path;
                }
                if (FileSaveType.QI_NIU.getType().equals(this.saveType)) {
                    this.urlPath = CommonLocalSetting.getCustomSetting().getConfigValue(SystemConfigName.qiniuDomain) + path.replace('\\', '/');
                }
            }
        }
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "fileId=" + fileId +
                ", fileName='" + fileName + '\'' +
                ", path='" + path + '\'' +
                ", fileType='" + fileType + '\'' +
                ", size=" + size +
                ", createTime=" + createTime +
                '}';
    }
}
