package com.site.blog.my.core.enums;

/**
 * 系统配置名称
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/12/15 16:59
 */
public enum SystemConfigName {
    footerAbout,
    footerCopyRight,
    footerICP,
    footerPoweredBy,
    footerPoweredByURL,
    themeName,
    websiteDescription,
    websiteIcon,
    websiteLogo,
    websiteName,
    yourAvatar,
    yourEmail,
    yourName,
    yourSign,
    qiniuAccessKey,
    qiniuSecretKey,
    qiniuDomain,
    qiniuBucket,
    fileSaveType,
    localFilePath,
    localFileDomain;
}
