package com.site.blog.my.core.config;


import cn.hutool.core.util.StrUtil;
import com.site.blog.my.core.constant.TbSystemConstant;
import com.site.blog.my.core.enums.SystemConfigName;
import com.site.blog.my.core.service.ConfigService;
import com.site.blog.my.core.util.SpringContextUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局缓存配置信息
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/25 10:57
 */
@Component
public class CommonLocalSetting {
    @Autowired
    private ConfigService configService;

    private Map<String, String> systemConfigs;

    private String themeName;


    /**
     * 获取全局自定义设置
     * @author xuwangcheng
     * @date 2019/6/3 12:13
     * @param
     * @return {@link CommonLocalSetting}
     */
    public static CommonLocalSetting getCustomSetting () {
        return SpringContextUtils.getBean(CommonLocalSetting.class);
    }

    /**
     *  更新缓存的配置信息
     * @author xuwangcheng
     * @date 2020/12/15 16:55
     * @param
     * @return
     */
    public void updateConfigs () {
        this.systemConfigs = configService.getAllConfigs(TbSystemConstant.SYSTEM_CONFIG_TYPE);
    }

    /**
     *  获取缓存的系统配置信息
     * @author xuwangcheng
     * @date 2020/12/15 17:00
     * @param configName configName
     * @return {@link String}
     */
    public String getConfigValue (SystemConfigName configName) {
        if (this.systemConfigs == null) {
            this.updateConfigs();
        }

        return this.systemConfigs.get(configName.name());
    }

    /**
     *  获取主题
     * @author xuwangcheng
     * @date 2020/3/25 21:43
     * @param
     * @return {@link String}
     */
    public String getTheme() {
        if (StrUtil.isBlank(themeName)) {
            themeName = configService.getAllConfigs(TbSystemConstant.SYSTEM_CONFIG_TYPE).get(SystemConfigName.themeName.name());
            if (StrUtil.isBlank(themeName)) {
                themeName = "amaze";
            }
        }

        return themeName;
    }

    public void setTheme(String theme) {
        this.themeName = theme;
    }
}
