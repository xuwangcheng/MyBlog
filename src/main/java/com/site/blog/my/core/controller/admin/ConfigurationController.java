package com.site.blog.my.core.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.site.blog.my.core.config.CommonLocalSetting;
import com.site.blog.my.core.constant.TbSystemConstant;
import com.site.blog.my.core.controller.blog.MyBlogController;
import com.site.blog.my.core.service.ConfigService;
import com.site.blog.my.core.util.FileUtil;
import com.site.blog.my.core.util.Result;
import com.site.blog.my.core.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author 13
 * @qq交流群 796794009
 * @email 2449207463@qq.com
 * @link http://13blog.site
 */
@Controller
@RequestMapping("/admin")
public class ConfigurationController {

    @Resource
    private ConfigService configService;
    @Resource
    private CommonLocalSetting commonLocalSetting;


    @GetMapping("/configurations")
    public String list(HttpServletRequest request) {
        request.setAttribute("path", "configurations");
        request.setAttribute("configurations", configService.getAllConfigs(TbSystemConstant.SYSTEM_CONFIG_TYPE));
        request.setAttribute("themeNames", FileUtil.getThemeNames());
        return "admin/configuration";
    }

    @PostMapping("/configurations/website")
    @ResponseBody
    public Result website(@RequestParam(value = "websiteName", required = false) String websiteName,
                          @RequestParam(value = "websiteDescription", required = false) String websiteDescription,
                          @RequestParam(value = "websiteLogo", required = false) String websiteLogo,
                          @RequestParam(value = "websiteIcon", required = false) String websiteIcon,
                          @RequestParam(value = "themeName", required = false) String themeName) {
        int updateResult = 0;
        if (!StringUtils.isEmpty(websiteName)) {
            updateResult += configService.updateConfig("websiteName", websiteName);
        }
        if (!StringUtils.isEmpty(websiteDescription)) {
            updateResult += configService.updateConfig("websiteDescription", websiteDescription);
        }
        if (!StringUtils.isEmpty(websiteLogo)) {
            updateResult += configService.updateConfig("websiteLogo", websiteLogo);
        }
        if (!StringUtils.isEmpty(websiteIcon)) {
            updateResult += configService.updateConfig("websiteIcon", websiteIcon);
        }

        if (!StringUtils.isEmpty(themeName)) {
            updateResult += configService.updateConfig("themeName", themeName);
            commonLocalSetting.setTheme(themeName);
        }
        // 更新系统缓存的配置项
        if (updateResult > 0) {
            commonLocalSetting.updateConfigs();
        }
        return ResultGenerator.genSuccessResult(updateResult > 0);
    }

    @PostMapping("/configurations/userInfo")
    @ResponseBody
    public Result userInfo(@RequestParam(value = "yourAvatar", required = false) String yourAvatar,
                           @RequestParam(value = "yourName", required = false) String yourName,
                           @RequestParam(value = "yourEmail", required = false) String yourEmail,
                           @RequestParam(value = "yourSign", required = false) String yourSign,
                           @RequestParam(value = "yourSubSign", required = false) String yourSubSign) {
        int updateResult = 0;
        if (!StringUtils.isEmpty(yourAvatar)) {
            updateResult += configService.updateConfig("yourAvatar", yourAvatar);
        }
        if (!StringUtils.isEmpty(yourName)) {
            updateResult += configService.updateConfig("yourName", yourName);
        }
        if (!StringUtils.isEmpty(yourEmail)) {
            updateResult += configService.updateConfig("yourEmail", yourEmail);
        }
        if (!StringUtils.isEmpty(yourSign)) {
            updateResult += configService.updateConfig("yourSign", yourSign);
        }
        if (!StringUtils.isEmpty(yourSubSign)) {
            updateResult += configService.updateConfig("yourSubSign", yourSubSign);
        }
        // 更新系统缓存的配置项
        if (updateResult > 0) {
            commonLocalSetting.updateConfigs();
        }
        return ResultGenerator.genSuccessResult(updateResult > 0);
    }

    @PostMapping("/configurations/footer")
    @ResponseBody
    public Result footer(@RequestParam(value = "footerAbout", required = false) String footerAbout,
                         @RequestParam(value = "footerICP", required = false) String footerICP,
                         @RequestParam(value = "footerCopyRight", required = false) String footerCopyRight,
                         @RequestParam(value = "footerPoweredBy", required = false) String footerPoweredBy,
                         @RequestParam(value = "footerPoweredByURL", required = false) String footerPoweredByURL) {
        int updateResult = 0;
        if (!StringUtils.isEmpty(footerAbout)) {
            updateResult += configService.updateConfig("footerAbout", footerAbout);
        }
        if (!StringUtils.isEmpty(footerICP)) {
            updateResult += configService.updateConfig("footerICP", footerICP);
        }
        if (!StringUtils.isEmpty(footerCopyRight)) {
            updateResult += configService.updateConfig("footerCopyRight", footerCopyRight);
        }
        if (!StringUtils.isEmpty(footerPoweredBy)) {
            updateResult += configService.updateConfig("footerPoweredBy", footerPoweredBy);
        }
        if (!StringUtils.isEmpty(footerPoweredByURL)) {
            updateResult += configService.updateConfig("footerPoweredByURL", footerPoweredByURL);
        }
        // 更新系统缓存的配置项
        if (updateResult > 0) {
            commonLocalSetting.updateConfigs();
        }
        return ResultGenerator.genSuccessResult(updateResult > 0);
    }


    /**
     *  更新文件设置
     * @author xuwangcheng
     * @date 2020/12/15 17:29
     * @param qiniuAccessKey qiniuAccessKey
     * @param qiniuSecretKey qiniuSecretKey
     * @param qiniuDomain qiniuDomain
     * @param qiniuBucket qiniuBucket
     * @param fileSaveType fileSaveType
     * @param localFilePath localFilePath
     * @param localFileDomain localFileDomain
     * @return {@link Result}
     */
    @PostMapping("/configurations/fileSetting")
    @ResponseBody
    public Result fileSetting (@RequestParam(value = "qiniuAccessKey", required = false) String qiniuAccessKey,
                               @RequestParam(value = "qiniuSecretKey", required = false) String qiniuSecretKey,
                               @RequestParam(value = "qiniuDomain", required = false) String qiniuDomain,
                               @RequestParam(value = "qiniuBucket", required = false) String qiniuBucket,
                               @RequestParam(value = "fileSaveType", required = false) String fileSaveType,
                               @RequestParam(value = "localFilePath", required = false) String localFilePath,
                               @RequestParam(value = "localFileDomain", required = false) String localFileDomain) {
        int updateResult = 0;
        if (!StringUtils.isEmpty(qiniuAccessKey)) {
            updateResult += configService.updateConfig("qiniuAccessKey", qiniuAccessKey);
        }
        if (!StringUtils.isEmpty(qiniuSecretKey)) {
            updateResult += configService.updateConfig("qiniuSecretKey", qiniuSecretKey);
        }
        if (!StringUtils.isEmpty(qiniuDomain)) {
            if (!qiniuDomain.endsWith("/")) {
                qiniuDomain += "/";
            }
            updateResult += configService.updateConfig("qiniuDomain", qiniuDomain);
        }
        if (!StringUtils.isEmpty(qiniuBucket)) {
            updateResult += configService.updateConfig("qiniuBucket", qiniuBucket);
        }
        if (!StringUtils.isEmpty(fileSaveType)) {
            updateResult += configService.updateConfig("fileSaveType", fileSaveType);
        }
        if (!StringUtils.isEmpty(localFilePath)) {
            updateResult += configService.updateConfig("localFilePath", localFilePath);
        }
        if (!StringUtils.isEmpty(localFileDomain)) {
            if (!localFileDomain.endsWith("/")) {
                localFileDomain += "/";
            }
            updateResult += configService.updateConfig("localFileDomain", localFileDomain);
        }
        // 更新系统缓存的配置项
        if (updateResult > 0) {
            commonLocalSetting.updateConfigs();
        }
        return ResultGenerator.genSuccessResult(updateResult > 0);

    }


}
