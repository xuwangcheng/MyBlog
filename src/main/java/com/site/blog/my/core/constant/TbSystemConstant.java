package com.site.blog.my.core.constant;

/**
 * 系统常量
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/12/17 10:54
 */
public interface TbSystemConstant {
    /**
     * 系统配置名称
     */
    String SYSTEM_CONFIG_TYPE = "system";
}
