package com.site.blog.my.core.dao;

import com.site.blog.my.core.entity.FileInfo;
import com.site.blog.my.core.util.PageQueryUtil;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/25 14:35
 */
@Component
public interface FileInfoMapper {
    int deleteByPrimaryKey(Integer fileId);

    int insert(FileInfo fileInfo);

    FileInfo selectByPrimaryKey(Integer fileId);
//
//    FileInfo selectByFileName(String fileName);
//
//    int updateByPrimaryKeySelective(FileInfo record);
//
//    int updateByPrimaryKey(FileInfo record);

    List<FileInfo> findFileList(PageQueryUtil pageUtil);

    int getFileInfoTotal (PageQueryUtil pageUtil);

    int deleteBatch(Integer[] ids);
//
//    int batchInsertFileInfo(List<FileInfo> fileList);

}
