package com.site.blog.my.core.service.impl;

import cn.hutool.core.io.IORuntimeException;
import com.site.blog.my.core.dao.FileInfoMapper;
import com.site.blog.my.core.entity.FileInfo;
import com.site.blog.my.core.service.FileInfoService;
import com.site.blog.my.core.util.FileUtil;
import com.site.blog.my.core.util.PageQueryUtil;
import com.site.blog.my.core.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/25 14:41
 */
@Service
public class FileInfoServiceImpl implements FileInfoService {

    @Autowired
    private FileInfoMapper fileInfoMapper;

    @Override
    public int saveFile(FileInfo fileInfo) {
        return fileInfoMapper.insert(fileInfo);
    }

    @Override
    public int deleteFileById(Integer fileId) {
        FileUtil.deleteSaveFile(fileInfoMapper.selectByPrimaryKey(fileId));
        return fileInfoMapper.deleteByPrimaryKey(fileId);
    }

    @Override
    public PageResult getFileInfoPage(PageQueryUtil pageUtil) {
        List<FileInfo> files = fileInfoMapper.findFileList(pageUtil);
        int total = fileInfoMapper.getFileInfoTotal(pageUtil);
        return new PageResult(files, total, pageUtil.getLimit(), pageUtil.getPage());
    }

    @Override
    public Boolean deleteBatch(Integer[] ids) {
        if (ids.length < 1) {
            return false;
        }

        for (Integer id: ids) {
            deleteFileById(id);
        }

        return true;
    }


}
