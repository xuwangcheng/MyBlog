package com.site.blog.my.core.service.impl;

import cn.hutool.core.util.StrUtil;
import com.site.blog.my.core.constant.TbSystemConstant;
import com.site.blog.my.core.dao.BlogConfigMapper;
import com.site.blog.my.core.entity.BlogConfig;
import com.site.blog.my.core.service.ConfigService;
import com.site.blog.my.core.util.MyBlogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Struct;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ConfigServiceImpl implements ConfigService {
    @Autowired
    private BlogConfigMapper configMapper;


    @Override
    public int updateConfig(String configName, String configValue) {
        BlogConfig blogConfig = configMapper.selectByPrimaryKey(configName);
        if (blogConfig != null) {
            blogConfig.setConfigValue(configValue);
            blogConfig.setUpdateTime(new Date());
            return configMapper.updateByPrimaryKeySelective(blogConfig);
        }
        return 0;
    }

    @Override
    public Map<String, String> getAllConfigs(String configType) {
        //获取所有的map并封装为map
        List<BlogConfig> blogConfigs = configMapper.selectAll(configType);
        Map<String, String> configMap = blogConfigs.stream().collect(Collectors.toMap(BlogConfig::getConfigName, new Function<BlogConfig, String>() {
            @Override
            public String apply(BlogConfig blogConfig) {
                String v = StrUtil.isBlank(blogConfig.getConfigValue()) ? blogConfig.getDefaultValue() : blogConfig.getConfigValue();
                return v == null ? "" : v;
            }
        }));
        return configMap;
    }
}
