package com.site.blog.my.core.service;

import com.site.blog.my.core.entity.FileInfo;
import com.site.blog.my.core.util.PageQueryUtil;
import com.site.blog.my.core.util.PageResult;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/25 14:40
 */
public interface FileInfoService {

    /**
     *  保存文件
     * @author xuwangcheng
     * @date 2020/3/25 14:45
     * @param fileInfo fileInfo
     * @return {@link int}
     */
    int saveFile (FileInfo fileInfo);

    /**
     * 删除文件
     * @author xuwangcheng
     * @date 2020/3/25 14:45
     * @param fileId fileId
     * @return {@link int}
     */
    int deleteFileById (Integer fileId);

    /**
     *  分页查询文件信息
     * @author xuwangcheng
     * @date 2020/3/25 15:04
     * @param pageUtil pageUtil
     * @return {@link PageResult}
     */
    PageResult getFileInfoPage(PageQueryUtil pageUtil);

    /**
     *  批量删除
     * @author xuwangcheng
     * @date 2020/3/25 17:52
     * @param ids ids
     * @return {@link Boolean}
     */
    Boolean deleteBatch(Integer[] ids);
}
