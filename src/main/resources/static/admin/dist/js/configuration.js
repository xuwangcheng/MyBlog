$(function () {
    //修改站点信息
    $('#updateWebsiteButton').click(function () {
        $("#updateWebsiteButton").attr("disabled", true);
        //ajax提交数据
        var params = $("#websiteForm").serialize();
        $.ajax({
            type: "POST",
            url: "/admin/configurations/website",
            data: params,
            success: function (result) {
                $("#updateWebsiteButton").removeAttr("disabled");
                if (result.resultCode == 200 && result.data) {
                    swal("保存成功", {
                        icon: "success",
                    });
                }
                else {
                    swal(result.message, {
                        icon: "error",
                    });
                }
                ;
            },
            error: function () {
                $("#updateWebsiteButton").removeAttr("disabled");
                swal("操作失败", {
                    icon: "error",
                });
            }
        });
    });
    //个人信息
    $('#updateUserInfoButton').click(function () {
        $("#updateUserInfoButton").attr("disabled", true);
        var params = $("#userInfoForm").serialize();
        $.ajax({
            type: "POST",
            url: "/admin/configurations/userInfo",
            data: params,
            success: function (result) {
                $("#updateUserInfoButton").removeAttr("disabled");
                if (result.resultCode == 200&& result.data) {
                    swal("保存成功", {
                        icon: "success",
                    });
                }
                else {
                    swal(result.message, {
                        icon: "error",
                    });
                }
                ;
            },
            error: function () {
                $("#updateUserInfoButton").removeAttr("disabled");
                swal("操作失败", {
                    icon: "error",
                });
            }
        });
    });
    //修改文件设置
    $('#updateFileSettingButton').click(function () {
        $("#updateFileSettingButton").attr("disabled", true);
        var params = $("#fileSettingForm").serialize();
        $.ajax({
            type: "POST",
            url: "/admin/configurations/fileSetting",
            data: params,
            success: function (result) {
                $("#updateFileSettingButton").removeAttr("disabled");
                if (result.resultCode == 200&& result.data) {
                    swal("保存成功", {
                        icon: "success",
                    });
                }
                else {
                    swal(result.message, {
                        icon: "error",
                    });
                }
                ;
            },
            error: function () {
                $("#updateFileSettingButton").removeAttr("disabled");
                swal("操作失败", {
                    icon: "error",
                });
            }
        });
    });
    //修改底部设置
    $('#updateFooterButton').click(function () {
        $("#updateFooterButton").attr("disabled", true);
        var params = $("#footerForm").serialize();
        $.ajax({
            type: "POST",
            url: "/admin/configurations/footer",
            data: params,
            success: function (result) {
                $("#updateFooterButton").removeAttr("disabled");
                if (result.resultCode == 200&& result.data) {
                    swal("保存成功", {
                        icon: "success",
                    });
                }
                else {
                    swal(result.message, {
                        icon: "error",
                    });
                }
                ;
            },
            error: function () {
                $("#updateFooterButton").removeAttr("disabled");
                swal("操作失败", {
                    icon: "error",
                });
            }
        });
    });

})
