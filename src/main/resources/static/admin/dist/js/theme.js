$(function () {
    //雪花特效
    $('[data-id="updateSettingButton"]').click(function () {
        var that = this;
        $(that).attr("disabled",true);
        var params = $(that).parents('form').serialize();
        $.ajax({
            type: "POST",
            url: "/admin/theme/updateConfig",
            dataType:'json',
            contentType: "application/json",
            data: JSON.stringify(urlParseMap(params)),
            success: function (result) {
                $(that).removeAttr("disabled");
                if (result.resultCode == 200 && result.data) {
                    swal("保存成功", {
                        icon: "success",
                    });
                }
                else {
                    swal(result.message, {
                        icon: "error",
                    });
                }
                ;
            },
            error: function () {
                $(that).removeAttr("disabled");
                swal("操作失败", {
                    icon: "error",
                });
            }
        });
    });
})
