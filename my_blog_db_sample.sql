/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql5.7
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : my_blog_db_sample

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 19/12/2020 17:37:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin_user`;
CREATE TABLE `tb_admin_user`  (
  `admin_user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `login_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员登陆名称',
  `login_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员登陆密码',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员显示昵称',
  `locked` tinyint(4) NULL DEFAULT 0 COMMENT '是否锁定 0未锁定 1已锁定无法登陆',
  PRIMARY KEY (`admin_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_admin_user
-- ----------------------------
INSERT INTO `tb_admin_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '白卡pala', 0);

-- ----------------------------
-- Table structure for tb_blog
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog`;
CREATE TABLE `tb_blog`  (
  `blog_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '博客表主键id',
  `blog_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客标题',
  `blog_sub_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客自定义路径url',
  `blog_cover_image` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客封面图',
  `blog_content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客内容',
  `blog_category_id` int(11) NOT NULL COMMENT '博客分类id',
  `blog_category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客分类(冗余字段)',
  `blog_tags` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客标签',
  `blog_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-草稿 1-发布',
  `blog_views` bigint(20) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `enable_comment` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-允许评论 1-不允许评论',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`blog_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog
-- ----------------------------
INSERT INTO `tb_blog` VALUES (59, '你好，世界', '', 'http://localhost:28083/admin/dist/img/rand/32.jpg', '这是一篇测试文章，你可以删除它', 39, '开发技术', 'java', 1, 0, 0, 0, '2020-12-19 17:35:34', '2020-12-19 17:35:34');

-- ----------------------------
-- Table structure for tb_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_category`;
CREATE TABLE `tb_blog_category`  (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类表主键',
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类的名称',
  `category_icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类的图标',
  `category_rank` int(11) NOT NULL DEFAULT 1 COMMENT '分类的排序值 被使用的越多数值越大',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_category
-- ----------------------------
INSERT INTO `tb_blog_category` VALUES (39, '开发技术', '/admin/dist/img/category/02.png', 2, 0, '2020-12-19 17:34:56');

-- ----------------------------
-- Table structure for tb_blog_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_comment`;
CREATE TABLE `tb_blog_comment`  (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `blog_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '关联的blog主键',
  `commentator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论者名称',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论人的邮箱',
  `website_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址',
  `comment_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论内容',
  `comment_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论提交时间',
  `commentator_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论时的ip地址',
  `reply_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '回复内容',
  `reply_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '回复时间',
  `comment_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否审核通过 0-未审核 1-审核通过',
  `is_deleted` tinyint(4) NULL DEFAULT 0 COMMENT '是否删除 0-未删除 1-已删除',
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_blog_tag
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tag`;
CREATE TABLE `tb_blog_tag`  (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签表主键id',
  `tag_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名称',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 184 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_tag
-- ----------------------------
INSERT INTO `tb_blog_tag` VALUES (183, 'java', 0, '2020-12-19 17:35:34');

-- ----------------------------
-- Table structure for tb_blog_tag_relation
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tag_relation`;
CREATE TABLE `tb_blog_tag_relation`  (
  `relation_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '关系表id',
  `blog_id` bigint(20) NOT NULL COMMENT '博客id',
  `tag_id` int(11) NOT NULL COMMENT '标签id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`relation_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 399 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_tag_relation
-- ----------------------------
INSERT INTO `tb_blog_tag_relation` VALUES (398, 59, 183, '2020-12-19 17:35:34');

-- ----------------------------
-- Table structure for tb_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_config`;
CREATE TABLE `tb_config`  (
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置项的名称',
  `config_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'system' COMMENT '配置类型,system-系统配置，其他为主题名称',
  `config_value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置项的值',
  `default_value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认值',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`config_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_config
-- ----------------------------
INSERT INTO `tb_config` VALUES ('aboutBgImg', 'amaze', 'https://images.xuwangcheng.com/1d29fd8b6255478eb380a09a5283343b.png', 'https://images.xuwangcheng.com/1d29fd8b6255478eb380a09a5283343b.png', '2020-12-19 10:33:27', '2020-12-19 14:20:43');
INSERT INTO `tb_config` VALUES ('aboutMeText', 'amaze', '我，白卡pala，苣蒻。\r\n                       <br>对，苣蒻，菜，菜爆了。\r\n<br><br>  行到水穷处，坐看云起时。', '我，Fly3949，苣蒻。\n                       <br>对，苣蒻，菜，菜爆了。', '2020-12-19 10:42:51', '2020-12-19 14:20:43');
INSERT INTO `tb_config` VALUES ('bgHeaderImg', 'amaze', 'https://images.xuwangcheng.com/bec8ba19336042ca85557b15aa483fff.jpg', '/blog/amaze/images/header.jpg', '2020-12-18 09:58:08', '2020-12-19 14:20:43');
INSERT INTO `tb_config` VALUES ('bgImg', 'amaze', 'https://images.xuwangcheng.com/a55191cc72d14858838302db110dd591.jpg', NULL, '2020-12-18 09:58:29', '2020-12-19 14:20:43');
INSERT INTO `tb_config` VALUES ('fileSaveType', 'system', '1', '1', '2020-12-15 16:22:43', '2020-12-19 17:28:55');
INSERT INTO `tb_config` VALUES ('footerAbout', 'system', '曾经的你', 'your personal blog. have fun.', '2018-11-11 20:33:23', '2020-12-15 15:40:05');
INSERT INTO `tb_config` VALUES ('footerCopyRight', 'system', '2017-2020@白卡pala', '@2018 十三', '2018-11-11 20:33:31', '2020-12-15 15:40:05');
INSERT INTO `tb_config` VALUES ('footerICP', 'system', '', '浙ICP备 xxxxxx-x号', '2018-11-11 20:33:27', '2020-12-15 15:40:05');
INSERT INTO `tb_config` VALUES ('footerPoweredBy', 'system', '白卡pala', 'personal blog', '2018-11-11 20:33:36', '2020-12-15 15:40:05');
INSERT INTO `tb_config` VALUES ('footerPoweredByURL', 'system', 'https://gitee.com/xuwangcheng/MyBlog', '##', '2018-11-11 20:33:39', '2020-12-15 15:40:05');
INSERT INTO `tb_config` VALUES ('localFileDomain', 'system', 'http://localhost:8099/myBlogResource/', NULL, '2020-12-15 17:19:17', '2020-12-19 17:28:55');
INSERT INTO `tb_config` VALUES ('localFilePath', 'system', 'F:\\resource\\myBlogResource\\', NULL, '2020-12-15 17:19:08', '2020-12-19 17:28:55');
INSERT INTO `tb_config` VALUES ('musicList', 'amaze', '[{\r\n                title: \"在希望的田野上\",\r\n                author: \"朴树\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2016/10/朴树-在希望的田野上.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/10/在希望的田野上-mp3-image.png\"\r\n            },{\r\n                title: \"听说\",\r\n                author: \"刘若英\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/刘若英-听说？.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \"Sincerely\",\r\n                author: \"TRUE\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/TRUE-Sincerely.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \"僕らの手には何もないけど、 (尽管我们的手中空无一物)\",\r\n                author: \"RAM WIRE (ラムワイヤー)\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/RAM-WIRE-ラムワイヤー-僕らの手には何もないけど、-尽管我们的手中空无一物.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \" 一辈子的孤单\",\r\n                author: \"刘若英\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/刘若英-一辈子的孤单.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \" 腻味\",\r\n                author: \"金玟岐\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/金玟岐-腻味.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            }\r\n            ]', '[{\r\n                title: \"在希望的田野上\",\r\n                author: \"朴树\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2016/10/朴树-在希望的田野上.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/10/在希望的田野上-mp3-image.png\"\r\n            },{\r\n                title: \"听说\",\r\n                author: \"刘若英\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/刘若英-听说？.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \"Sincerely\",\r\n                author: \"TRUE\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/TRUE-Sincerely.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \"僕らの手には何もないけど、 (尽管我们的手中空无一物)\",\r\n                author: \"RAM WIRE (ラムワイヤー)\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/RAM-WIRE-ラムワイヤー-僕らの手には何もないけど、-尽管我们的手中空无一物.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \" 一辈子的孤单\",\r\n                author: \"刘若英\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/刘若英-一辈子的孤单.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            },{\r\n                title: \" 腻味\",\r\n                author: \"金玟岐\",\r\n                url: \"https://young.xuwangcheng.com/wp-content/uploads/2019/12/金玟岐-腻味.mp3\",\r\n                pic: \"https://young.xuwangcheng.com/wp-content/uploads/2016/09/听说？-mp3-image.png\"\r\n            }\r\n            ]', '2020-12-18 22:26:23', '2020-12-19 14:20:43');
INSERT INTO `tb_config` VALUES ('musicPlayer', 'amaze', '1', '0', '2020-12-18 22:12:19', '2020-12-19 14:20:43');
INSERT INTO `tb_config` VALUES ('qiniuAccessKey', 'system', '', NULL, '2020-12-15 16:05:51', '2020-12-18 17:24:55');
INSERT INTO `tb_config` VALUES ('qiniuBucket', 'system', '', NULL, '2020-12-15 16:06:41', '2020-12-18 17:24:55');
INSERT INTO `tb_config` VALUES ('qiniuDomain', 'system', '', NULL, '2020-12-15 16:06:27', '2020-12-18 17:24:55');
INSERT INTO `tb_config` VALUES ('qiniuSecretKey', 'system', '', NULL, '2020-12-15 16:06:11', '2020-12-18 17:24:55');
INSERT INTO `tb_config` VALUES ('snowColor', 'amaze', '#FFFFFF', '#5ECDEF', '2020-12-17 15:54:44', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowCount', 'amaze', '150', '50', '2020-12-17 15:57:41', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowflakes', 'amaze', '1', '0', '2020-12-17 15:42:26', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowMaxOpacity', 'amaze', '1', '0.95', '2020-12-18 11:16:57', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowMaxSize', 'amaze', '20', '18', '2020-12-17 15:58:01', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowMinOpacity', 'amaze', '0.7', '0.6', '2020-12-18 11:16:53', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowMinSize', 'amaze', '8', '8', '2020-12-17 15:57:54', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowRotation', 'amaze', '1', '1', '2020-12-18 11:17:36', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowSpeed', 'amaze', '1', '1', '2020-12-17 15:57:29', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('snowWind', 'amaze', '1', '1', '2020-12-18 11:17:55', '2020-12-18 17:23:47');
INSERT INTO `tb_config` VALUES ('themeName', 'system', 'amaze', 'amaze', '2020-03-25 21:24:17', '2020-12-17 14:34:34');
INSERT INTO `tb_config` VALUES ('websiteDescription', 'system', '这是属于小白的个人博客,基于springboot+mybaits+thymeleaf+mysql开发的博客程序', 'personal blog是SpringBoot2+Thymeleaf+Mybatis建造的个人博客网站.SpringBoot实战博客源码.个人博客搭建', '2018-11-11 20:33:04', '2020-12-17 14:34:34');
INSERT INTO `tb_config` VALUES ('websiteIcon', 'system', '/admin/dist/img/favicon.png', '/admin/dist/img/favicon.png', '2018-11-11 20:33:11', '2020-12-17 14:34:34');
INSERT INTO `tb_config` VALUES ('websiteLogo', 'system', '/admin/dist/img/logo2.png', '/admin/dist/img/logo2.png', '2018-11-11 20:33:08', '2020-12-17 14:34:34');
INSERT INTO `tb_config` VALUES ('websiteName', 'system', '曾经的你', 'personal blog', '2018-11-11 20:33:01', '2020-12-17 14:34:34');
INSERT INTO `tb_config` VALUES ('yourAvatar', 'system', 'https://images.xuwangcheng.com/e8be7d104ffc48e89bdf8598201f24c1.jpg', '/admin/dist/img/13.png/admin/dist/img/13.png', '2018-11-11 20:33:14', '2020-12-18 17:28:09');
INSERT INTO `tb_config` VALUES ('yourEmail', 'system', 'xuwangcheng14@163.com', '2449207463@qq.com', '2018-11-11 20:33:17', '2020-12-18 17:28:09');
INSERT INTO `tb_config` VALUES ('yourName', 'system', '白卡pala', '十三', '2018-11-11 20:33:20', '2020-12-18 17:28:09');
INSERT INTO `tb_config` VALUES ('yourSign', 'system', '此刻•少年', '我不怕千万人阻挡，只怕自己投降！', '2020-12-17 14:36:50', '2020-12-18 17:28:09');
INSERT INTO `tb_config` VALUES ('yourSubSign', 'system', '背灯和月就花阴，已是十年踪迹十年心', '行到水穷处，坐看云起时', '2020-12-18 17:34:17', '2020-12-18 17:34:17');

-- ----------------------------
-- Table structure for tb_file_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_file_info`;
CREATE TABLE `tb_file_info`  (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `path` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `save_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '2' COMMENT '保存类型， 1 - 本地 2 -七牛云',
  `size` bigint(20) NULL DEFAULT NULL,
  `create_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '文件信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_file_info
-- ----------------------------
INSERT INTO `tb_file_info` VALUES (46, '头部背景.jpg', 'bec8ba19336042ca85557b15aa483fff.jpg', 'jpg', '2', 124979, '2020-12-18 17:26:55');
INSERT INTO `tb_file_info` VALUES (47, '主题背景.jpg', 'a55191cc72d14858838302db110dd591.jpg', 'jpg', '2', 668874, '2020-12-18 17:27:19');
INSERT INTO `tb_file_info` VALUES (48, '头像.jpg', 'e8be7d104ffc48e89bdf8598201f24c1.jpg', 'jpg', '2', 61752, '2020-12-18 17:27:40');
INSERT INTO `tb_file_info` VALUES (49, 'about背景图.png', '1d29fd8b6255478eb380a09a5283343b.png', 'png', '2', 2510657, '2020-12-19 09:51:57');

-- ----------------------------
-- Table structure for tb_link
-- ----------------------------
DROP TABLE IF EXISTS `tb_link`;
CREATE TABLE `tb_link`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '友链表主键id',
  `link_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '友链类别 0-友链 1-推荐 2-个人网站',
  `link_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站名称',
  `link_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站链接',
  `link_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站描述',
  `link_rank` int(11) NOT NULL DEFAULT 0 COMMENT '用于列表排序',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-未删除 1-已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`link_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
