# MyBlog
基于SpringBoot+Thymeleaf开发的个人博客程序，是学习SpringBoot的入门项目，欢迎Star和Fork.

#### 介绍
源仓库地址：https://github.com/ZHENFENG13/My-Blog  
原作者博客：http://13blog.site/

### 二次改造和开发
管理账号 admin 123456

### 更新记录
- 增加文件管理模块，使用nginx或者七牛云管理静态资源;
- 可在后台手动选择博客主题，增加了主题设置模块;
- 美化了默认主题Amaze;
- 增加了音乐播放器;
- 代码更新及一些小BUG修复;


### 后续更新计划
- 增加清风明月模块（类似说说）；
- 增加相册模块；
- 增加音乐模块；
- 新增基于pjax+amaze开发的主题；
- 增加插件开发逻辑；
- 代码优化。